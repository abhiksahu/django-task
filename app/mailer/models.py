#-*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.db import models
from django.db.models import Count, Sum
from django.utils.encoding import python_2_unicode_compatible


class Company(models.Model):
    name = models.CharField(max_length=150)
    bic = models.CharField(max_length=150, blank=True)

    def get_order_count(self):
        # orders = 0
        # for order in self.orders.all():
        #     orders += 1
        # return orders
        # print(self.contacts.all().count())
        # print(self.orders.all().count())
        # print(self.orders.aggregate(models.Count("order_number"))['order_number__count'])
        # if self.orders.only('id').aggregate(Count("id"))['id__count'] != orders:
        #     print('company - 1', False)
        return self.orders.only('id').aggregate(Count("id"))['id__count']

    def get_order_sum(self):
        # total_sum = 0
        # for contact in self.contacts.all():
        #     for order in contact.orders.all():
        #         total_sum += order.total
        # return total_sum
        # print(self.contacts.aggregate(models.Count('orders'))['orders__count'])
        # print(self.orders.aggregate(models.Sum('total'))['total__sum'])
        # if self.orders.only('total').aggregate(Sum('total'))['total__sum'] != total_sum:
        #     print('company - 2', False)
        return self.orders.only('total').aggregate(Sum('total'))['total__sum']


class Contact(models.Model):
    company = models.ForeignKey(
        Company, related_name="contacts", on_delete=models.PROTECT)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=150, blank=True)
    email = models.EmailField()

    def get_order_count(self):
        # orders = 0
        # for order in self.orders.all():
        #     orders += 1
        # print(self.orders.aggregate(models.Count('id'))['id__count'])
        # print(self.orders.all()[0])
        return self.orders.only('id').aggregate(Count('id'))['id__count']


@python_2_unicode_compatible
class Order(models.Model):
    order_number = models.CharField(max_length=150)
    company = models.ForeignKey(Company, related_name="orders")
    contact = models.ForeignKey(Contact, related_name="orders")
    total = models.DecimalField(max_digits=18, decimal_places=9)
    order_date = models.DateTimeField(null=True, blank=True)
    # for internal use only
    added_date = models.DateTimeField(auto_now_add=True)
    # for internal use only
    modified_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        # print(self.pk)
        return "%s" % self.order_number
